#! /usr/bin/ python3.5
import sys

def run(argv):
   # Set up path to import from settings
   if (settings.ALWAYS_FAIL):
      print("{} failed to execute.".format(argv[0]))
   else:
      print("{} ran successfully!".format(argv[0]))

if __name__ == "__main__":
   if __package__ is None:
      from os import path
      sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
      from settings import settings
   run(sys.argv)
