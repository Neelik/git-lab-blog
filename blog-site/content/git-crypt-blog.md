Title: Configuring Git-crypt To Enable Storing Sensitive Information In A Git Repository
Date: 2020-03-29 15:20
Category: Blog

## Introduction

I recently started working on a Django project that had extensive settings files,
several of which were dedicated to sensitve information. Things like API keys
for external services and secret keys for various servers. Our deploy system is
fully automated, requiring quick access to this sensitive information which in turn
means being able to put those values into a repository.

I'm sure many of you are aware that putting sensitive information in plain text
over the internet is a less than stellar idea. However, having those values readily
available would really help streamline our deploys. So, how do you securely store
values in a repository? Enter the [git-crypt package.](https://github.com/AGWA/git-crypt)

## What is git-crypt?

The git-crypt package is a handy little tool that lets you leverage RSA keys to
encrypt and decrypt sensitive information, allowing you to safely store it in a
git repository. It just so happens to be a perfect solution to the problem I
currently face on my project.

From here forward, this blog will read like a walkthrough of setting your repository
to work with git-crypt, allowing you to freely commit like nothing ever changed.

## Installation of git-crypt

> **WARNING** This walkthrough was designed for a Linux system.

Installing git-crypt is a little trickier than a standard package. Begin by cloning
the repository linked in the introduction:

```bash
    $ git clone git@github.com:AGWA/git-crypt.git
```

Navigate inside the project directory and build the project:

```bash
    $ make
    $ make install
```

For more advanced installation options or to build the project with the documentation,
reference the packages [installation instructions](https://github.com/AGWA/git-crypt/blob/master/INSTALL.md).

After successfully installing git-crypt, the next step is getting a key setup.
This key will be created using the GNU privacy guard, or `gpg`.

## Generating a new GPG Key

Depending on your distribution, the `gnupg` tool should be installed by default. If,
for some reason, it is not you can see instructions on how to [install gnupg on Linux](https://www.gnupg.org/howtos/card-howto/en/ch02.html).

Once you have the tool installed, the next step is generating a new key. Issue the
following command in your terminal:

```bash
    $ gpg --gen-key
```

This will launch a series of prompts guiding you through the creation of the key.

![Step 1]({static}/images/gpg-gen-key-1.png "gpg --gen-key output sample")

The first prompt asks what encryption type you want to use for the key. I opted
for the first option, RSA and RSA. This is also the default, so you can just hit
`<enter>` or enter 1 explicitly, your choice.

![Step 2]({static}/images/gpg-gen-key-2.png "gpg --gen-key output sample")

After selecting what type of key you'd like, you'll be asked to select the number
of bits to be used. The default is 2048, but I opted for the max length of 4096.
This also happens to be the recommended length by GitHub.

![Step 3]({static}/images/gpg-gen-key-3.png "gpg --gen-key output sample")

Once you've chosen your key length, it's time to choose an expiration.
The default here is "0", meaning the key never expires, which is what I opted for. 
You will be prompted to confirm your expiration choice before moving on to
the next step.

![Step 4]({static}/images/gpg-gen-key-4.png "gpg --gen-key output sample")

The last step is to enter indentifying information for your key along with a
passphrase. There are three pieces of identifying information required: a name,
an email (GitHub recommends it to be the account email associated with the
repository being secured), and a comment. The comment is just a brief description
about the purpose of the key.

After entering the three pieces of identifying information, you'll be prompted
to enter and re-enter a passphrase. My system used a separate window pop-up for
this so I do not have a screencap of that step.

![Step 5]({static}/images/gpg-gen-key-5.png "gpg --gen-key output sample")

Finally, the command spits out the information about the finalized key.

### **System Entropy**

![Entropy]({static}/images/key-entropy.png "MOAR BITS!!")

You may see a prompt like above requesting more entropy to generate the key. To
satisfy this, just browse the internet, chat with friends in Discord and/or Slack,
or click through some files. You just need to perform a variety of actions for the
system to generate enough entropy to create a satisfactory key. As you can see from
the image, I had to repeat this a few times as the bytes required actually *increased*.

##  Initializing git-crypt In Your Repository

As a quick recap, you should now have git-crypt installed and a new gpg key
generated for use. If not, please refer back to the previous sections to make
sure you have what you need to continue.

The next step is initializing git-crypt inside your repository. Navigate into the
root directory of your repository and submit the following command:

```bash
    $ git-crypt init
```

Once initialized, a `.gitattributes` file is needed to tell git-crypt which files
you want encrypted. Place the file in the root of your project.

> **WARNING** The `.gitattributes` file needs to be defined BEFORE committing the
> files intended for encryption.

The syntax for the file is similar to a `.gitignore` file. Below is the sample
of my version for this blog:

```gitattributes
    settings/settings.py filter=git-crypt diff=git-crypt
```

## Connecting git-crypt To Your Repository With the GPG Key

The final step needed to tie this all together is to link your gpg key to your
repository so that git-crypt knows you are allowed to encrypt/decrypt the sensitive
files. This is done with a single command, which will automatically commit the key
into your repository:

```bash
    git-crypt add-gpg-user FAFA1386
```

You'll want to replace the `FAFA1386` above with the ID for your public key. If
you didn't save it from the generation, that's ok. There's an easy way to retrieve
it.

```bash
    gpg --list-keys
```

You will see output like in the image below:

![List Keys]({static}/images/gpg-list-keys-sample.png "gpg --list-keys output")

## Conclusion

That's it! Your repository is now set up with git-crypt, enabling you to securely
store sensitive API keys and settings in your repository. You don't have to do
anything fancy after this, just commit and push like you normally would. The
git-crypt package does all the heavy lifting under the hood.


## Additional Reading

[1. GNU Privacy Handbook](https://www.gnupg.org/gph/en/manual/c14.html)

[2. Installing xsltproc Dependency for git-crypt MAN Pages](https://zoomadmin.com/HowToInstall/UbuntuPackage/xsltproc)

[3. git-crypt on GitHub](https://github.com/AGWA/git-crypt)
